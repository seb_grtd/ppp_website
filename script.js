/** Boutton pour retourner en haut de la page */
window.onscroll = function () {
    if (window.scrollY<600) {document.getElementById("button-back-container").style.display = "none";}
    else {document.getElementById("button-back-container").style.display = "block";}
}

/** Hamburger menu */

const hamburger = document.querySelector(".hamburger");
const navMenu = document.querySelector(".top-bar-container > nav > ul");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
}
